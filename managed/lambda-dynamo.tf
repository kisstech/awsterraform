data "aws_iam_policy_document" "dynamo_lambda_policy_cyril" {

  statement {
    sid = "AllowCreatingLogGroups"
    effect = "Allow"
    resources = [
      "arn:aws:logs:*:*:*"]
    actions = [
      "logs:CreateLogGroup"]
  }
  statement {
    sid = "AllowWritingLogs"
    effect = "Allow"
    resources = [
      "arn:aws:logs:*:*:log-group:/aws/lambda/*:*"]

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
  }
  statement {
    sid = "AllowWritingDynamo"
    effect = "Allow"
    resources = [
      "arn:aws:dynamodb:*:*:*"]

    actions = [
      "dynamodb:*"
    ]
  }
}

resource "aws_iam_policy" "dynamo_lambda_policy_cyril" {
  policy = "${data.aws_iam_policy_document.dynamo_lambda_policy_cyril.json}"
}

resource "aws_iam_role" "iam_for_lambda_dynamodb_cyril" {
  name = "iam_for_lambda_dynamodb_cyril"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "dynamo_lambda_policy_attachment_cyril" {
  policy_arn = "${aws_iam_policy.dynamo_lambda_policy_cyril.arn}"
  role = "${aws_iam_role.iam_for_lambda_dynamodb_cyril.name}"
}

resource "aws_lambda_function" "lambda_cyril_dynamo" {
  function_name = "CyrilDynamo"
  filename = "lambda_function_payload.zip"
  runtime = "nodejs8.10"

  # "main" is the filename within the zip file (hello.js) and "handler"
  # is the name of the property under which the handler function was
  # exported in that file.
  handler = "dynamo.handler"

  source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"

  role = "${aws_iam_role.iam_for_lambda_dynamodb_cyril.arn}"

  environment {
    variables = {
      environment = "${var.environment}"
      dynamodb_name = "${var.dynamodb_name}"
    }
  }
}
