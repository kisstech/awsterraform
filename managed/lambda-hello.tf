resource "aws_iam_role" "iam_for_lambda" {
  name = "cyril_iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "lambda_cyril_hello" {
  function_name = "CyrilHello"
  filename = "lambda_function_payload.zip"
  runtime = "nodejs8.10"

  # "main" is the filename within the zip file (hello.js) and "handler"
  # is the name of the property under which the handler function was
  # exported in that file.
  handler = "hello.handler"

  source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"

  role = "${aws_iam_role.iam_for_lambda.arn}"

  environment {
    variables = {
      environment = "${var.environment}"
    }
  }
}
