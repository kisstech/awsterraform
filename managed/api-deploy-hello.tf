
resource "aws_api_gateway_deployment" "cyril_api_hello_deploy" {
  depends_on = [
    "aws_api_gateway_integration.cyril_api_hello_get_lambda"
  ]


  rest_api_id = "${aws_api_gateway_rest_api.cyril_api_hello.id}"
  stage_name  = "dev"
}

resource "aws_lambda_permission" "cyril_api_hello_lambda_permission" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_cyril_hello.function_name}"
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_deployment.cyril_api_hello_deploy.execution_arn}/*/*"
}

# API Gateway Output #

output "base_url_api_hello" {
  value = "${aws_api_gateway_deployment.cyril_api_hello_deploy.invoke_url}"
}
