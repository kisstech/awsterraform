resource "aws_sqs_queue" "sqs_cyril" {
  name = "${var.sqs_name}"
}

output "sqs_cyril_id" {
  value = "${aws_sqs_queue.sqs_cyril.id}"
}
