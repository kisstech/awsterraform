// Load the AWS SDK for Node.js
const AWS = require('aws-sdk');
// Set the region
AWS.config.update({region: 'eu-west-1'});

// Create the DynamoDB service object
const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

exports.handler = async (event) => {
    return new Promise((resolve, reject) => {
        // const contentObj = JSON.parse(event);
        const pmid = event.pmid;
        const email = event.email;
        const phone = event.phone;

        const params = {
            TableName: process.env.dynamodb_name,
            Item: {
                'pmid': {S: pmid},
                'email': {S: email},
                'phone': {S: phone}
            }
        };

        // Call DynamoDB to add the item to the table
        ddb.putItem(params, function (err, data) {
            if (err) {
                reject("Error: " + err);
                console.log("Error", err);
            } else {
                console.log("Success", data);

                const response = {
                    statusCode: 200,
                    body: JSON.stringify('PUTOK'),
                };
                resolve(response);
            }
        });
    });
};
