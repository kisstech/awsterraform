exports.handler = function (event, context) {

    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello ' + process.env.environment + ' \n event => ' + JSON.stringify(event)),
        headers: { "Set-Cookie": "HELLO=" + Math.random() }
    };
    context.succeed(response);

    // context.succeed('hello world: ' + process.env.environment);
};
