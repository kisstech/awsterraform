resource "aws_dynamodb_table" "dynamodb-cyril-users" {
  name = "${var.dynamodb_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "pmid"

  attribute {
    name = "pmid"
    type = "S"
  }

  attribute {
    name = "email"
    type = "S"
  }

  attribute {
    name = "phone"
    type = "S"
  }

  global_secondary_index {
    name = "email-index"
    hash_key = "email"
    write_capacity = 10
    read_capacity = 10
    projection_type = "ALL"
  }

  global_secondary_index {
    name = "phone-index"
    hash_key = "phone"
    write_capacity = 10
    read_capacity = 10
    projection_type = "ALL"
  }

  tags = {
    Name = "dynamodb-table-cyril"
    Environment = "${var.environment}"
  }
}
