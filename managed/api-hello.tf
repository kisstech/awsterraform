
resource "aws_cloudwatch_log_group" "cyril_cloudwatch_lambda_hello" {
  name              = "/aws/lambda/${aws_lambda_function.lambda_cyril_hello.function_name}"
  retention_in_days = 14
}


resource "aws_api_gateway_rest_api" "cyril_api_hello" {
  name        = "Cyril API Hello"
  description = "Simple Hello World API"
}

resource "aws_api_gateway_resource" "cyril_api_hello" {
  rest_api_id = "${aws_api_gateway_rest_api.cyril_api_hello.id}"
  parent_id   = "${aws_api_gateway_rest_api.cyril_api_hello.root_resource_id}"
  path_part   = "hello"
}

resource "aws_api_gateway_method" "cyril_api_hello_get" {
  rest_api_id   = "${aws_api_gateway_rest_api.cyril_api_hello.id}"
  resource_id   = "${aws_api_gateway_resource.cyril_api_hello.id}"
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "cyril_api_hello_get_lambda" {
  rest_api_id = "${aws_api_gateway_rest_api.cyril_api_hello.id}"
  resource_id = "${aws_api_gateway_method.cyril_api_hello_get.resource_id}"
  http_method = "${aws_api_gateway_method.cyril_api_hello_get.http_method}"

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.lambda_cyril_hello.invoke_arn}"

}

resource "aws_api_gateway_method_response" "cyril_api_hello_get_200" {
  rest_api_id = "${aws_api_gateway_rest_api.cyril_api_hello.id}"
  resource_id = "${aws_api_gateway_resource.cyril_api_hello.id}"
  http_method = "${aws_api_gateway_method.cyril_api_hello_get.http_method}"
  status_code = "200"

  response_models {
    "text/html" = "Empty"
  }
}
