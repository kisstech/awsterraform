resource "aws_iam_role" "lambda_cyril_sqs_trigger" {
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "sqs_cyril_trigger" {
  policy_arn = "${aws_iam_policy.sqs_cyril_trigger.arn}"
  role = "${aws_iam_role.lambda_cyril_sqs_trigger.name}"
}

resource "aws_iam_policy" "sqs_cyril_trigger" {
  policy = "${data.aws_iam_policy_document.sqs_cyril_trigger.json}"
}

data "aws_iam_policy_document" "sqs_cyril_trigger" {
  statement {
    sid = "AllowSQSPermissions"
    effect = "Allow"
    resources = [
      "arn:aws:sqs:*"]

    actions = [
      "sqs:ChangeMessageVisibility",
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes",
      "sqs:ReceiveMessage",
    ]
  }

  statement {
    sid = "AllowInvokingLambdas"
    effect = "Allow"
    resources = [
      "arn:aws:lambda:*:*:function:*"]
    actions = [
      "lambda:InvokeFunction"]
  }

  statement {
    sid = "AllowCreatingLogGroups"
    effect = "Allow"
    resources = [
      "arn:aws:logs:*:*:*"]
    actions = [
      "logs:CreateLogGroup"]
  }
  statement {
    sid = "AllowWritingLogs"
    effect = "Allow"
    resources = [
      "arn:aws:logs:*:*:log-group:/aws/lambda/*:*"]

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
  }
  statement {
    sid = "AllowWritingDynamo"
    effect = "Allow"
    resources = [
      "arn:aws:dynamodb:*:*:*"]

    actions = [
      "dynamodb:*"
    ]
  }
}

resource "aws_lambda_event_source_mapping" "lambda_cyril_sqs_event_source_mapping" {
  batch_size        = 1
  event_source_arn  = "${aws_sqs_queue.sqs_cyril.arn}"
  enabled           = true
  function_name     = "${aws_lambda_function.lambda_cyril_sqs.arn}"
}

resource "aws_lambda_function" "lambda_cyril_sqs" {
  function_name = "CyrilSqs"
  filename = "lambda_function_payload.zip"
  runtime = "nodejs8.10"

  # "main" is the filename within the zip file (sqs.js) and "handler"
  # is the name of the property under which the handler function was
  # exported in that file.
  handler = "sqs.handler"

  source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"

  role = "${aws_iam_role.lambda_cyril_sqs_trigger.arn}"

  environment {
    variables = {
      environment = "${var.environment}"
    }
  }
}
