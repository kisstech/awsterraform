provider "aws" {
  profile = "formation-awsterraform"
  region = "eu-west-1"
}

variable "environment" {
  default = "sandbox"
}

variable "dynamodb_name" {
  default = "CyrilUsers"
}

variable "sqs_name" {
  default = "cyril"
}
