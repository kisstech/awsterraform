#!/usr/bin/env bash
cd website
aws s3 sync . s3://formation-websitessl-cyril.kiss.university/ --profile kiss-formation
cd ..
