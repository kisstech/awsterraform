locals {
  s3_origin_id = "${var.website_domain_name}"
}

resource "aws_cloudfront_distribution" "www_distribution" {
  origin {
    // We need to set up a "custom" origin because otherwise CloudFront won't
    // redirect traffic from the root domain to the www domain
    custom_origin_config {
      // These are all the defaults.
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }

    domain_name = "${aws_s3_bucket.website.website_endpoint}"
    origin_id   = "${local.s3_origin_id}"
  }

  // Example another origin (api gateway)
//  origin {
//    custom_origin_config {
//      // These are all the defaults.
//      http_port              = "80"
//      https_port             = "443"
//      origin_protocol_policy = "https-only"
//      origin_ssl_protocols   = ["TLSv1.2"]
//    }
//
//    origin_id   = "${local.api_origin_id}"
//
//    domain_name = "${local.api_origin_id}"
//    origin_path = "/${var.api_stage_name}"
//  }

  enabled             = true
  default_root_object = "index.html"

  //  logging_config {
  //    include_cookies = false
  //    bucket          = "mylogs.s3.amazonaws.com"
  //    prefix          = "myprefix"
  //  }

  aliases = ["${var.website_domain_name}"]

  // API Gateway
//  ordered_cache_behavior {
//    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
//    cached_methods = ["GET", "HEAD", "OPTIONS"]
//    path_pattern = "/login"
//    target_origin_id = "${local.api_origin_id}"
//    viewer_protocol_policy = "https-only"
//    forwarded_values {
//      query_string = true
//      headers = ["Accept", "Referer", "Authorization", "Content-Type"]
//      cookies {
//        forward = "all"
//      }
//    }
//    compress = true
//  }

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = "${local.s3_origin_id}"

    forwarded_values {
      query_string = true
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  price_class = "PriceClass_200"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Environment = "Formation"
  }

  viewer_certificate {
//    cloudfront_default_certificate = true
    acm_certificate_arn = "${aws_acm_certificate.certificate.arn}"
    minimum_protocol_version = "TLSv1"
    ssl_support_method  = "sni-only"
  }
}

output "cloudfront_domainname" {
  value = "${aws_cloudfront_distribution.www_distribution.domain_name}"
}
