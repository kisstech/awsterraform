provider "aws" {
  alias = "virginia"
  region = "us-east-1"
  profile = "kiss-formation"
}

// Create certificate in US Virignia (to use with Cloud Front)
resource "aws_acm_certificate" "certificate" {
  provider = "aws.virginia"
  domain_name       = "${var.website_domain_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "cert_validation" {
  name    = "${aws_acm_certificate.certificate.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.certificate.domain_validation_options.0.resource_record_type}"
  zone_id = "${var.aws_route53_zone_id}"
  records = ["${aws_acm_certificate.certificate.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "cert" {
  provider = "aws.virginia"
  certificate_arn = "${aws_acm_certificate.certificate.arn}"
  validation_record_fqdns = ["${aws_route53_record.cert_validation.fqdn}"]
}
