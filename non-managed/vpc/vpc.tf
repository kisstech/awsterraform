
resource "aws_vpc" "hellovpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = "Hello VPC"
  }
}

resource "aws_route53_zone" "private" {
  name = "${var.private_dns}"
  tags = {
    Name = "Private"
  }
  vpc {
    vpc_id = "${aws_vpc.hellovpc.id}"
  }
}

/*
resource "aws_route53_zone_association" "private" {
  vpc_id = "${aws_vpc.hellovpc.id}"
  zone_id = "${aws_route53_zone.private.id}"
}*/

resource "aws_subnet" "private" {
  cidr_block = "10.0.1.0/24"
  vpc_id = "${aws_vpc.hellovpc.id}"
  tags = {
    Name = "Subnet Private"
  }
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true
}

/*
resource "aws_subnet" "public" {
  cidr_block = "10.0.2.0/24"
  vpc_id = "${aws_vpc.hellovpc.id}"
  tags {
    Name = "Subnet Public"
  }
  availability_zone = "eu-west-1c"
  map_public_ip_on_launch = true
}
*/

resource "aws_security_group" "intervpc" {
  name = "Inter VPC"
  description = "Allowing instances communication within vpc"
  vpc_id = "${aws_vpc.hellovpc.id}"
  tags = {
    "Name" = "intervpc"
  }

  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [ "${aws_vpc.hellovpc.cidr_block}" ]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [ "${aws_vpc.hellovpc.cidr_block}" ]
  }
}

resource "tls_private_key" "hellokey" {
  algorithm = "RSA"

}

resource "aws_key_pair" "hellokey" {
  key_name = "hellovpc"
  public_key = "${tls_private_key.hellokey.public_key_openssh}"
}

resource "local_file" "pem" {
  content = "${tls_private_key.hellokey.private_key_pem}"
  filename = "hellovpc"

  provisioner "local-exec" {
    command = "chmod 400 hellovpc && mv -f hellovpc ~/.ssh/"
  }
}

# enable external (internet) access
resource "aws_internet_gateway" "gateway" {
  vpc_id = "${aws_vpc.hellovpc.id}"
  tags = {
    "Name" = "main"
  }
}

resource "aws_route_table" "route_table" {
  vpc_id = "${aws_vpc.hellovpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gateway.id}"
  }
}

/*
resource "aws_route_table_association" "main" {
  route_table_id = "${aws_route_table.route_table.id}"
  subnet_id = "${aws_subnet.public.id}"
}*/

resource "aws_route_table_association" "private" {
  route_table_id = "${aws_route_table.route_table.id}"
  subnet_id = "${aws_subnet.private.id}"
}

output "vpc_id" {
  value = "${aws_vpc.hellovpc.id}"
}

/*
output "public_subnet_id" {
  value = "${aws_subnet.public.id}"
}*/

output "private_subnet_id" {
  value = "${aws_subnet.private.id}"
}

output "security_group_ids" {
  value = [ "${aws_security_group.intervpc.id}" ]
}

/*
output "subnet_ids" {
  value = [ "${aws_subnet.private.id}", "${aws_subnet.public.id}" ]
}*/

output "key_name" {
  value = "${aws_key_pair.hellokey.key_name}"
}

output "private_key_pem" {
  value = "${tls_private_key.hellokey.private_key_pem}"
}

output "zone_id" {
  value = "${aws_route53_zone.private.id}"
}

output "private_dns" {
  value = "${var.private_dns}"
}