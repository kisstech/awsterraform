variable "ami" {
  default = "ami-030dbca661d402413"
}

variable "dse_count" {
  default = "3"
}

variable "private_dns" {}
variable "zone_id" {}
variable "key_name" {}
variable "private_key" {}
variable "vpc_security_group_ids" {}
variable "subnet_id" {}
