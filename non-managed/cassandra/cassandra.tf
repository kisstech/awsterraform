
resource "aws_instance" "node" {
  count = "${var.dse_count}"
  ami = "${var.ami}"
  instance_type = "t2.xlarge"
  vpc_security_group_ids = "${var.vpc_security_group_ids}"
  key_name = "${var.key_name}"
  subnet_id = "${var.subnet_id}"
  tags = {
    "Name" = "DSE ${count.index + 1}"
  }
}

/*
resource "aws_ebs_volume" "volume" {
  count = "${var.count}"
  availability_zone = "${element(aws_instance.node.*.availability_zone, count.index)}"
  size = 512
  tags {
    "Name" = "POCIP DSE volume ${count.index + 1}"
  }
}

resource "aws_volume_attachment" "attachment" {
  count = "${var.count}"
  device_name = "/dev/xvdf"
  instance_id = "${element(aws_instance.node.*.id, count.index)}"
  volume_id = "${element(aws_ebs_volume.volume.*.id, count.index)}"
  force_detach = true
}
*/

resource "null_resource" "config" {
  count = "${var.dse_count}"
  //depends_on = ["aws_volume_attachment.attachment" ]

  connection {
    host = "${element(aws_instance.node.*.public_ip, count.index)}"
    type = "ssh"
    agent = false
    user = "ec2-user"
    private_key = "${var.private_key}"
  }

  provisioner "file" {
    source = "${path.module}/resources/"
    destination = "/tmp"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y && sudo yum install -y java-1.8.0-openjdk-devel.x86_64",
      "sudo chmod +x /tmp/config-node.sh && sudo /tmp/config-node.sh ${element(aws_instance.node.*.private_ip, count.index)}"
    ]
  }
}

resource "aws_route53_record" "record" {
  count = "${var.dse_count}"
  zone_id = "${var.zone_id}"
  name    = "cassandra${count.index + 1}.${var.private_dns}"
  type    = "A"
  ttl     = "5"
  records = [ "${element(aws_instance.node.*.private_ip, count.index)}" ]
}
