#!/usr/bin/env bash

ip=$1

mv /tmp/cassandra.repo /etc/yum.repos.d/

yum install -y cassandra

sed -e 's/\${private_dns}/'${ip}'/' /tmp/cassandra.yaml > /etc/cassandra/conf/cassandra.yaml

systemctl daemon-reload && systemctl start cassandra

# create schema
while ! cqlsh ${ip} -e 'describe cluster' > /dev/null
do
    sleep 3
    echo 'cassandra still starting ... '
done

cqlsh ${ip} -f /tmp/schema.cql

