
resource "aws_instance" "application" {
  ami = "${var.ami}"
  instance_type = "t2.large"
  vpc_security_group_ids = "${var.vpc_security_group_ids}"
  key_name = "${var.key_name}"
  subnet_id = "${var.subnet_id}"
  tags = {
    "Name" = "Java Application"
  }
}

resource "null_resource" "config" {
  connection {
    host = "${aws_instance.application.public_dns}"
    type = "ssh"
    agent = false
    user = "ec2-user"
    private_key = "${var.private_key}"
  }

  provisioner "file" {
    source = "${path.module}/resources/"
    destination = "/tmp"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y && sudo yum install -y java-1.8.0-openjdk-devel.x86_64",
      "sudo chmod +x /tmp/install-app.sh && sudo /tmp/install-app.sh "
    ]
  }
}

resource "aws_route53_record" "record" {
  zone_id = "${var.zone_id}"
  name    = "java.${var.private_dns}"
  type    = "A"
  ttl     = "5"
  records = [ "${aws_instance.application.private_ip}" ]
}

resource "aws_route53_record" "public" {
  zone_id = "${var.zone_id_public}"
  name    = "java.${var.public_dns}"
  type    = "A"
  ttl     = "5"
  records = [ "${aws_instance.application.public_ip}" ]
}

output "public_dns" {
  value = "${aws_instance.application.*.public_dns}"
}
