#!/usr/bin/env bash

# Configure app
mv /tmp/app.jar /opt/app.jar
mv /tmp/start.sh /opt/ && chmod +x /opt/start.sh

# Install as a Service
mv /tmp/app.service /etc/systemd/system/app.service && chmod 644 /etc/systemd/system/app.service

# start service
systemctl enable app.service
service app start
