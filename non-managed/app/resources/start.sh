#!/usr/bin/env bash

export SPRING_CASSANDRA_CONTACT_POINTS=cassandra1.kiss.university
export SPRING_CASSANDRA_KEYSPACE=hellotf

java -jar /opt/app.jar --logging.file=/var/log/app.log &
