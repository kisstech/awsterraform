variable "ami" {
  default = "ami-030dbca661d402413"
}

variable "vpc_security_group_ids" { }

variable "key_name" {
  default = ""
}

variable "subnet_id" {
  default = ""
}

variable "private_key" {
  default = ""
}

variable "zone_id" {
  default = ""
}

variable "private_dns" {
  default = ""
}
variable "zone_id_public" {
  default = ""
}

variable "public_dns" {
  default = ""
}

