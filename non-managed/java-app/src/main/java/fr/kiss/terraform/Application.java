package fr.kiss.terraform;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@SpringBootApplication
@EnableCassandraRepositories
public class Application {

  @Value("${spring.cassandra.contact_points}")
  String[] cassandraContactPoints;

  @Value("${spring.cassandra.keyspace}")
  String keyspace;

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Bean
  Session session() {
    Cluster cluster = Cluster.builder()
      .withoutJMXReporting()
      .addContactPoints(cassandraContactPoints).build();
    return cluster.connect(keyspace);
  }

}
