package fr.kiss.terraform;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Formation {

  @PrimaryKey
  private String id;

  private String name;
}
