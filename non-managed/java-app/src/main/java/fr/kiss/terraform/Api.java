package fr.kiss.terraform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.core.query.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class Api {

  @Autowired
  CassandraTemplate cassandraTemplate;

  @GetMapping("/formations")
  List<Formation> formations() {
    return cassandraTemplate.select(Query.query(Query.empty()).withAllowFiltering(), Formation.class);
  }

}
