provider aws {
  region  = "eu-west-1"
}

module "vpc" {
  source = "./vpc"
}

module "cassandra" {
  source = "./cassandra"

  zone_id = "${module.vpc.zone_id}"
  private_dns = "${module.vpc.private_dns}"
  key_name = "${module.vpc.key_name}"
  private_key = "${module.vpc.private_key_pem}"
  vpc_security_group_ids = ["${module.vpc.security_group_ids}", "${var.me_sg}"]
  subnet_id = "${module.vpc.private_subnet_id}"
}

module "app" {
  source = "./app"

  zone_id = "${module.vpc.zone_id}"
  zone_id_public = "${var.zone_id_public}"
  private_dns = "${module.vpc.private_dns}"
  key_name = "${module.vpc.key_name}"
  private_key = "${module.vpc.private_key_pem}"
  vpc_security_group_ids = ["${module.vpc.security_group_ids}", "${var.me_sg}"]
  subnet_id = "${module.vpc.private_subnet_id}"
}
