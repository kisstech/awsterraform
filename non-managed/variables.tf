variable "environment" {
  default = "sandbox"
}

variable "dse_count" {
  default = "3"
}

variable "private_dns" {
  default = "kiss.university"
}

variable "me_sg" {
  default = "sg-0c94f6052196be9e2"
}

variable "zone_id_public" {
  default = "Z2FPOXX2B702O3"
}
