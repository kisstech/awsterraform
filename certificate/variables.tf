provider "aws" {
  region  = "us-east-1"
  profile = "kiss-formation"
}

variable "website_domain_name" {
  default = "certificate-cyril-formation.kiss.university"
}

variable "aws_route53_zone_id" {
  default = "Z36JAKDSS900LS"
}
