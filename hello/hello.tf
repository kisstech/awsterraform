provider "aws" {
  region  = "eu-west-1"
}

variable "website_bucket_name" {
  default = "s3-website-hello-cyril.kiss.fr"
}

resource "aws_s3_bucket" "website" {
  bucket = "${var.website_bucket_name}"
  acl    = "public-read"

  policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "PublicReadForGetBucketObjects",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.website_bucket_name}/*"
    }
  ]
}
EOF

  website {
    index_document = "index.html"
    error_document = "error.html"

    routing_rules = <<EOF
[{
    "Condition": {
        "KeyPrefixEquals": "docs/"
    },
    "Redirect": {
        "ReplaceKeyPrefixWith": "documents/"
    }
}]
EOF
  }
}

output "website_url" {
  value = "http://${aws_s3_bucket.website.website_endpoint}"
}
